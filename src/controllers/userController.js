const {successAction, failAction} = require('../utilities/response');
const {save, deleteOne} = require('../services/userServices');

module.exports = {
    userSave: async (req, res) => {
        try {
            const data = await save(req.body);
            res.status(200).json(successAction(data, "User saved successfully"));
        } catch (error) {
            res.status(400).json(failAction(error.message));
        }
    },
    userDelete: async (req, res) => {
        try {
            const {id} = req.params;
            const data = await deleteOne(id);
            res.status(200).json(successAction(null, "User deleted successfully"));
        } catch (error) {
            res.status(400).json(failAction(error.message));
        }
    }
};

const authorizationCode = "sugarBoxTestKey"
const {failAction} = require('../utilities/response');
module.exports = {
    checkAuthorizationCode: async function (req, res, next) {
        const {authorization} = req.headers;
        if (!authorization)return res.status(400).json(failAction("authorization code is required"));
        try {
            if (authorizationCode !== authorization)return  res.status(400).json(failAction("authorization code is expired"));
            next();

        } catch (ex) {
            return res.status(400).json(failAction("error is occur"));
        }
    }
};

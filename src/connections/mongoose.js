const mongoose = require('mongoose');
const mongoUrl = "";
// plz add your mongo url

mongoose.connect(mongoUrl, {  useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false, });
mongoose.connection
    .on('connected', () =>{
        console.log('connected to database' )})
    .on('error', err => console.log('database error: ' + err));

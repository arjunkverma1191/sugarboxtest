const UserModel = require('../collections/user');
const {encryptPassword} = require('../utilities/helper')

module.exports = {
    save: async (entry) => {
        const {email, password} = entry;
        if (await UserModel.findOneByCondition({email})) throw new Error("user already exist with email")
        let entryToCreate={
            ...entry,
            isDeleted: false,
            createdAt: Date.now(),
            password: await encryptPassword(password)
        };
        return await UserModel.saveUser(entryToCreate);
    },
    deleteOne: async (userId) => {
        let data = await UserModel.findByConditionAndUpdate({_id: userId}, {
            isDeleted: true,
            deletedAt: Date.now()
        }, {new: true});
        if(!data) throw new Error("User not found");
        return ;
    }
};

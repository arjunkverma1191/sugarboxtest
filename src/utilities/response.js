module.exports = {
    successAction: (data, message = 'OK') => {
        return ({statusCode: 200, data, message});
    },
    failAction: (message = 'Fail', statusCode = 400) => {
        return ({statusCode, data: null, message});
    }
};
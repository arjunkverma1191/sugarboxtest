const mongoose = require('mongoose')
const dbSchema = require("./db-schema");

class UserClass {


  static findOneByCondition(entry) {
    let filter={
      ...entry,
      isDeleted: false
    };
    return this.findOne(filter);
  }
  static saveUser(payload) {
    return this(payload).save();
  }
  static findByConditionAndUpdate(entry, payload, options) {
    let filter={
      ...entry,
      isDeleted: false
    };
    return this.findOneAndUpdate(filter, payload, options);
  }
}

dbSchema.loadClass(UserClass);

module.exports=mongoose.model("User", dbSchema);

const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        trim: true,
        required: true,
    },
    password: {type: String},
    createdAt: Number,
    isDeleted: {type: Boolean, default: false},
    deletedAt: Number

});
module.exports= userSchema;

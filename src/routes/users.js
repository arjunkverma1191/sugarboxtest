const express = require('express');
const router = express.Router();
const {createValidator} = require('express-joi-validation');
const Joi = require('@hapi/joi');
const validator = createValidator({passError: true});
const {userSave, userDelete} = require('../controllers/userController')

const saveSchema = Joi.object({
    email: Joi.string().required().label("email"),
    password: Joi.string().required().label("password")
});

router.post('/',validator.body(saveSchema, {
    joi: { convert: true, allowUnknown: false }
}), userSave);


router.delete('/:id', userDelete);

module.exports = router;

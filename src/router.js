const express = require('express');
const router = express.Router();

const usersRouter = require('./routes/users');
const {checkAuthorizationCode} = require('./middleware/authorize');

router.use('/user',checkAuthorizationCode, usersRouter);

module.exports = router;